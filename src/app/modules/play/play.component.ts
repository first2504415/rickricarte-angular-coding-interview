import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      })
    )).subscribe();

  results: string = '';
  everyQuestionMarked: boolean = true;
  correctAnswers: number = 0;
  wrongAnswers: number = 0;
  amount: number = 0;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService,
  ) { }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        this.amount = params.amount;
      });
    
    this.questions$.subscribe(questions => {
      if (questions.find(q => !q.selectedId)) {
        this.everyQuestionMarked = false;
      } else {
        this.everyQuestionMarked = true;

        this.correctAnswers = questions
          .filter(question => question.selectedId == question.answers.find(answer => answer.isCorrect)?._id)
          .length;
      
        this.wrongAnswers = questions
          .filter(question => question.selectedId != question.answers.find(answer => answer.isCorrect)?._id)
          .length;

        this.results = `Correct answers: ${this.correctAnswers} | Wrong answers: ${this.wrongAnswers}`;
      }
    });
  }

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  }

}
